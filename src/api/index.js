// 3P
import { Container } from 'typedi';

// APP
import UserRouter from './UserRouter';

export default ({ expressApp }) => {
  expressApp.use('/user', UserRouter);
}