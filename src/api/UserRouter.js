// 3P
import { Container } from 'typedi';
import express from 'express';

// APP
import UserController from '../controllers/UserController';

const router = express.Router();
const controller = Container.get(UserController);

router.get('/list', controller.listUsers);
router.get('/:userId', controller.getUserById);
router.get('/profile', controller.getProfile);
// router.post('/profile', controller.updateProfile);

export default router;