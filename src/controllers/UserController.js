// 3P
import { Container } from "typedi";

// APP
import UserService from "../services/UserService";


export default class UserController {

  constructor() {
    this.userService = Container.get(UserService);
  }

  listUsers = async (req, res) => {
    try {
      const users = await this.userService.listUsers();

      return res.json({
        success: true,
        body: {
          users,
        },
      });
    } catch (err) {
      return res.json({
        success: false,
        error: err.message,
      });
    }
  }

  getUserById = async (req, res, next) => {
    try {
      const { userId } = req.params;
      const user = await this.userService.getUser(userId);

      return res.json({
        success: true,
        body: {
          user,
        },
      });
    } catch (err) {
      return res.json({
        success: false,
        error: err.message,
      });
    }
  }

  getProfile = async (req, res, next) => {
    try {
      const userId = 1; // req.ctx.auth.userId;
      const profile = await this.userService.getProfile(userId);
      
      return res.json({
        success: true,
        body: {
          profile,
        }
      })
    } catch (err) {
      return res.json({
        success: false,
        error: err.message,
      });
    }
  }

}