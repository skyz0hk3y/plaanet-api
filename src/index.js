import 'module-alias/register';

// 3P
import { Container } from 'typedi';
import express from 'express';

// APP
import { Config } from './config';
import loaders from './loaders';
import api from './api';

async function main() {
  // Create an express app
  const app = express();

  // Let's init loaders & add endpoints
  await loaders({ expressApp: app });
  await api({ expressApp: app });

  // Now that loaders are setup, we can listen
  app.listen(Config.http.port, err => {
    const logger = Container.get('Logger');

    if (err) {
      logger.error(`Cannot start HTTP server. Error: ${err.message}`);
      process.exit(1);
    }

    logger.info(`HTTP Server is listening on http://${Config.http.host}:${Config.http.port}`);
  });
}

main();