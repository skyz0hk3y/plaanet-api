import config from 'config'

export const Config = {
  database: {
    host: config.get('database.host'),
    port: config.get('database.port'),
    user: config.get('database.user'),
    pass: config.get('database.pass'),
    schema: config.get('database.name')
  },
  http: {
    host: config.get('http.host'),
    port: config.get('http.port'),
  },
};