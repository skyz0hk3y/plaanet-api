// 3P
import { Container } from 'typedi';

import cors from 'cors';
import bodyParser from 'body-parser';

// APP

export default async ({ ctx, app }) => {
  // Add some app health endpoints (load-balancing)
  app.get('/status', (_, res) => res.status(200).end('OK'));
  app.head('/status', (_, res) => res.status(200).end());

  // Trust proxy (NGinx, Apache, HaProxy, etc)
  app.enable('trust proxy');

  // Global middlewares
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // Register express app into DI container
  Container.set('HttpServer', app);

  return app;
}