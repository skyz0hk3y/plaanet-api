// 3P
import Container from 'typedi';

// APP
import loggerLoader from './logger';
import httpLoader from './http-server';
import databaseLoader from './database';

export default async ({ expressApp }) => {
  try {
    const logger = await loggerLoader();
    Container.set('Logger', logger);

    await databaseLoader();
    logger.info('Connected to database!');

    await httpLoader({ app: expressApp });
    logger.info('Initialized HTTP server...');

    // More loaders here...
    // Initialise agenda.js, redis, etc
  } catch (err) {
    console.error('Loader error:', err.message);
  }
};