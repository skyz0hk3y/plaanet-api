// 3P
import { Container } from 'typedi';
import mongoose from 'mongoose';

// APP
import { Config } from '../config';

export default async () => {
  // Build mongodb URL
  const dbUrl = `mongodb://${Config.database.host}:${Config.database.port}/${Config.database.name}`;

  // Connect to mongo
  const connection = await mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  // Register database handle to DI container
  const dbHandle = connection.connection.db;
  Container.set('Database', dbHandle);

  // Return mongoose ORM object
  return dbHandle;
}

