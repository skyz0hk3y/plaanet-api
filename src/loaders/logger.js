// 3P
import bunyan from 'bunyan';

export default async () => {
  // Create bunyan logger
  const logger = bunyan.createLogger({
    name: 'plaanet-api-poc',
  });

  return logger;
}