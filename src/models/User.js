import { Schema, model } from 'mongoose';

export const UserSchema = new Schema({
  name: String,
  email: String,
  password: String,
  referrerUsername: String,
  referredUsersCount: {
    type: Number,
    default: 0,
  },
  accessToken: String,
  isActive: Boolean,
  isAdmin: Boolean,
  pages: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Page'
    }
  ],
  wallets: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Wallet'
    }
  ],
  contacts: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Wallet'
    }
  ]
})

export const User = model('User', UserSchema);