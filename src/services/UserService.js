// 3P
// import Container from "typedi";

// APP
import { User } from '../models/User';

class UserService {
  constructor() { }

  async listUsers() {
    const users = await User.find().exec();

    if (!users) {
      return [];
    }

    return users;
  }

  async getUser(userId) {
    const user = await User
      .findOne({ type: 'user', id: userId })
      .select('-password')
      .exec();

    if (!user) {
      throw new Error(`User does not exists. (uid=${userId})`);
    }

    return user;
  }

  async getProfile(userId) {
    const user = await this.getUser(userId);
    if (!user) {
      throw new Error(`User does not exists. (uid=${userId})`);
    }

    const profile = await UserProfile.findOne({ userId }).exec();
    if (!profile) {
      throw new Error(`No profile belonging to User. (uid=${userId})`);
    }

    return profile;
  }
}

export default UserService;