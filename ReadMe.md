# Plaanet API (PoC)

This repository aims to provide a minimal example (proof-of-concept) of
what I mean about re-structuring the plaanet/plaanet-api codebase.

## Start dev server

```shell
# First clone this repository
$ git clone https://gitlab.com/skyz0hk3y/plaanet-api.git plaanet-api-poc

# Enter the project
$ cd plaanet-api-poc/

# Start the developer web server
$ yarn start:dev
```
